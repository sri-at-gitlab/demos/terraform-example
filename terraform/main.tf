variable "access_key" {
    type = string
    default = "Undefined environment variable $TF_VARS_access_key"
}

variable "secret_key" {
    type = string
    default = "Undefined environment variable $TF_VARS_secret_key"
}

variable "region" {
    type = string
    default = "Undefined environment variable $TF_VARS_region"
}

variable "ami_ubuntu_18_04_lts" {
    type = string
    default = "Undefined environment variable $TF_VARS_ami_ubuntu_18_04_lts"
}

variable "instance_type" {
    type = string
    default = "Undefined environment variable $TF_VARS_instance_type"
}

provider "aws" {
    access_key = var.access_key
    secret_key = var.secret_key
    region = var.region
}

resource "aws_instance" "ec2_ubuntu" {
    ami = var.ami_ubuntu_18_04_lts
    instance_type = var.instance_type
    associate_public_ip_address = true
    user_data = file("../deployment_script/main.sh")
}

output "aws_instance_ec2_ubuntu" {
    value = aws_instance.ec2_ubuntu
}
