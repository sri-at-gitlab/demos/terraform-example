# Terraform Example

In this example, we trigger Terraform pipelines thru GitLab pipelines. Additionally it shows how to publish `terraform plan` output back into the merge request.

## Sample Merge Request 

Check out [this MR](https://gitlab.com/sri-at-gitlab/demos/terraform-example/-/merge_requests/3) as a sample that shows results of `terraform plan` [published as a comment](https://gitlab.com/sri-at-gitlab/demos/terraform-example/-/merge_requests/3#note_411627648).

## Components

- [Plan job](https://gitlab.com/sri-at-gitlab/demos/terraform-example/-/blob/refactor-branch/pipeline/terraform.gitlab-ci.yml#L26) executes `terraform plan` and stores the output as `terraform/plan.publish_to_mr.txt`
- [Publish job](https://gitlab.com/sri-at-gitlab/demos/terraform-example/-/blob/refactor-branch/pipeline/terraform.gitlab-ci.yml#L54) executes the publish script
- [pipeline/publish_to_mr.py](https://gitlab.com/sri-at-gitlab/demos/terraform-example/-/blob/refactor-branch/pipeline/publish_to_mr.py) is a Python script that publishes the  `terraform/plan.publish_to_mr.txt` file to the MR
