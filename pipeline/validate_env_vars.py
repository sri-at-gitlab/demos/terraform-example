import os

if 'ENV' not in os.environ:
    print('$ENV needs to be either DEV, TEST or PROD')
    exit(1)

env = str(os.environ['ENV']).upper()

if env not in ['DEV', 'TEST', 'PROD']:
    print('$ENV needs to be either DEV, TEST or PROD')
    exit(1)

prefix = env + '_'

expected = ['access_key', 'secret_key', 'region', 'ami_ubuntu_18_04_lts', 'instance_type']

for name in expected:
    key = prefix + name
    if key not in os.environ:
        print('$' + key + ' needs to be defined')
        exit(1)
